
/**
 * @author Casey Richter
 *
 */
public class Recursion {
    public static void main(String[] args) {
        System.out.println(powerN(3, 1));
        System.out.println(powerN(3, 2));
        System.out.println(powerN(3, 3));
        System.out.println(triangle(0));
        System.out.println(triangle(1));
        System.out.println(triangle(2));
    }

    /**
     * Given base and n that are both 1 or more, computes recursively the value
     * of base to the n power.
     * 
     * @param base base number.
     * @param n    power number.
     * @return integer output.
     */
    public static int powerN(int base, int n) {
        if (n == 1) {
            return base;
        }
        return base * powerN(base, n - 1);
    }

    /**
     * Computes recursively the total number of blocks in a triangle with the
     * given number of rows.
     * 
     * @param row number of rows.
     * @return number of blocks in triangle.
     */
    public static int triangle(int row) {
        if (row == 0) {
            return 0;
        }
        return row + triangle(row - 1);
    }

}
